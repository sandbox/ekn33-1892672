
Module: GTM (Google Tag Manager for Drupal)
Maintainer: eule <http://drupal.org/user/395032>
Co-Maintainer: drupalrv <http://drupal.org/user/2277548>

Description
===========
Adds the Google Tag Manager Code to your Site.

Requirements
============

* Google Tag Manager user account


Installation
============
* Copy the 'gtm' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your Google Tag Manager Code snippet.

You can also limit which user roles are tracked by ticking the
appropriate role(s).

All pages will now have the required JavaScript added to the node.
